/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.model.KonfigurasiModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.*;

import java.io.File;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class ProfileActivity extends AppCompatActivity {

    public PopHelper ph;
    public ImageView myImage, imgProfile, imgIdCard;
    public EditText inpName, inpEmail, inpPhone;
    public TextView btnUpdate, btnSaveIdCard, btnUpdatePassword, btnKeluar, btnUbahBahasa, btnSyaratDanKetentuan, btnKebijakanPrivasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        ph = new PopHelper(this);
        reqKonfigurasi();
        _initView();
    }

    public void _initView(){
        myImage = findViewById(R.id.imgQR);
        inpName = findViewById(R.id.inpName);
        inpEmail = findViewById(R.id.inpEmail);
        inpPhone = findViewById(R.id.inpPhone);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnSaveIdCard = findViewById(R.id.btnSaveIdCard);
        btnUpdatePassword = findViewById(R.id.btnUpdatePassword);
        imgProfile = findViewById(R.id.imgProfile);
        imgIdCard = findViewById(R.id.imgIdCard);
        btnKeluar = findViewById(R.id.btnKeluar);
        btnUbahBahasa = findViewById(R.id.btnUbahBahasa);
        btnSyaratDanKetentuan = findViewById(R.id.btnSyaratDanKetentuan);
        btnKebijakanPrivasi = findViewById(R.id.btnKebijakanPrivasi);

        _actionView();
    }

    public void _actionView(){

        inpName.setText(UserHelper.user.name);
        inpEmail.setText(UserHelper.user.email);
        inpPhone.setText(UserHelper.user.phone);

        Glide.with(this)
                .load(UserHelper.user.avatar)
                .apply(new RequestOptions().circleCrop())
                .apply(new RequestOptions().placeholder(R.color.white).error(R.color.white))
                .into(imgProfile);

        Glide.with(this)
                .load(UserHelper.user.id_card)
                .apply(new RequestOptions().placeholder(R.color.white).error(R.color.white))
                .into(imgIdCard);

        imgProfile.setOnClickListener(view -> {
            newImgParam = imgProfile;
            choosePhoto();
        });

        imgIdCard.setOnClickListener(view -> {
            newImgParam = imgIdCard;
            choosePhoto();
        });

        btnUpdate.setOnClickListener(view -> {
            if(uriProfil == null){
                reqUpdateProfile();
            }else {
                reqUpdateAvatar();
//                reqUpdateProfile();
            }
        });

        btnSaveIdCard.setOnClickListener(view -> {
            if(uriIdCard == null){
                GeneralHelper.ToastNew(this, "Choose Image first!");
            }else {
                reqUpdateIdCard();
            }
        });

        btnUpdatePassword.setOnClickListener(view -> {
            new GeneralHelper().intentAct(this, NewPasswordActivity.class);
        });

        btnSyaratDanKetentuan.setOnClickListener(view -> {
            Intent u = new Intent(ProfileActivity.this, DetailTermAndPoliceActivity.class);
            u.putExtra("title", "" + getResources().getString(R.string.menu_syarat_dan_ketentuan));
            u.putExtra("detail", km.sdk);
            startActivity(u);
        });

        btnKebijakanPrivasi.setOnClickListener(view -> {
            Intent u = new Intent(ProfileActivity.this, DetailTermAndPoliceActivity.class);
            u.putExtra("title", "" + getResources().getString(R.string.menu_kebijakan_privasi));
            u.putExtra("detail", km.kdp);
            startActivity(u);
        });

        btnUbahBahasa.setOnClickListener(view -> {
            Dialog d = new Dialog(ProfileActivity.this);
            d.setContentView(R.layout.pop_indo_inggris);
            TextView btnIndo = d.findViewById(R.id.btnIndo);
            TextView btnEnglish = d.findViewById(R.id.btnEnglish);

            btnIndo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setLocale("in");
                    d.dismiss();
                    startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
                    finish();
                }
            });

            btnEnglish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setLocale("en");
                    d.dismiss();
                    startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
                    finish();
                }
            });

            d.show();
        });

        btnKeluar.setOnClickListener(view -> {
            Dialog d = new Dialog(ProfileActivity.this);
            d.setContentView(R.layout.pop_ya_tidak);
            TextView btnYa = d.findViewById(R.id.btnYa);
            TextView btnTidak = d.findViewById(R.id.btnTidak);

            btnYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PreferencesHelper.setRegisteredUser(getApplicationContext(),null);
                    PreferencesHelper.setRegisteredPass(getApplicationContext(),null);

                    d.dismiss();
                    finish();
                }
            });

            btnTidak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    d.dismiss();
                }
            });

            d.show();
        });
    }

    public KonfigurasiModel km;

    @SuppressLint("CheckResult")
    public void reqKonfigurasi(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqKonfigurasi(
                        Config.CODE_APP + "",
                        PreferencesHelper.getKeyLanguage(ProfileActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                km = new KonfigurasiModel();
                                km = respData.data;
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        PreferencesHelper.setKeyLanguage(ProfileActivity.this, lang);
    }

    private static final int PICK_IMAGE = 1;
    private static final int PERMISSION_REQUEST_STORAGE = 2;
    private Uri uriProfil, uriIdCard, uri;
    public ImageView newImgParam;

    private void choosePhoto() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        }else{
            openGallery();
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if(data != null) {

                if(newImgParam == imgProfile){
                    uriProfil = data.getData();
                    uri = uriProfil;
                    Glide.with(this)
                            .load(uriProfil)
                            .apply(new RequestOptions().circleCrop())
                            .apply(new RequestOptions().placeholder(R.color.purple).error(R.color.black))
                            .into(imgProfile);
                }else {
                    uriIdCard = data.getData();
                    uri = uriIdCard;
                    Glide.with(this)
                            .load(uriIdCard)
                            .apply(new RequestOptions().placeholder(R.color.purple).error(R.color.black))
                            .into(imgIdCard);
                }

                try{
                    File file = FileUtils.getFile(com.sebaya.bejaji.ProfileActivity.this, uri);
                }catch (Exception e){
                    uri = null;
                    if(newImgParam == imgProfile){
                        uriProfil = null;
                    }else {
                        uriIdCard = null;
                    }
                    Toast.makeText(com.sebaya.bejaji.ProfileActivity.this, "File not found!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                }
                return;
            }
        }
    }

    private void reqUpdateAvatar() {
        ph.popLoading();
        File file = FileUtils.getFile(com.sebaya.bejaji.ProfileActivity.this, uriProfil);

        RequestBody photoBody = null;
        MultipartBody.Part photoPart = null;

        if(file != null){
            photoBody = RequestBody.create(MediaType.parse("*/*"), file);
            photoPart = MultipartBody.Part.createFormData("avatar",
                    file.getName(), photoBody);
        }

        RequestBody codeApp = RequestBody.create(MediaType.parse("text/plain"), Config.CODE_APP);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), UserHelper.user.id);
        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), UserHelper.user.token);

        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqUpdateAvatar(
                codeApp,
                id,
                token,
                photoPart
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData  -> {
                            UserHelper.user = respData.data;
                            uriProfil = null;
                            Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            ph.popDismiss();
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                            ph.popDismiss();
                        }
                );
    }


    private void reqUpdateIdCard() {
        ph.popLoading();
        File file = FileUtils.getFile(com.sebaya.bejaji.ProfileActivity.this, uriIdCard);

        RequestBody photoBody = null;
        MultipartBody.Part photoPart = null;

        if(file != null){
            photoBody = RequestBody.create(MediaType.parse("*/*"), file);
            photoPart = MultipartBody.Part.createFormData("id_card",
                    file.getName(), photoBody);
        }

        RequestBody codeApp = RequestBody.create(MediaType.parse("text/plain"), Config.CODE_APP);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), UserHelper.user.id);
        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), UserHelper.user.token);

        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqUpdateIdCard(
                codeApp,
                id,
                token,
                photoPart
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData  -> {
                            UserHelper.user = respData.data;
                            uriIdCard = null;
                            Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            ph.popDismiss();
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                            ph.popDismiss();
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void reqUpdateProfile(){
        ph.popLoading();
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqUpdateProfile(
                Config.CODE_APP + "",
                inpName.getText().toString().trim() + "",
                UserHelper.user.token + "",
                UserHelper.user.id + "",
                inpPhone.getText().toString().trim() + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                UserHelper.user = respData.data;
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                            ph.popDismiss();
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                            ph.popDismiss();
                        }
                );
    }
}