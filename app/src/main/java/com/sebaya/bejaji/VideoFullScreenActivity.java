package com.sebaya.bejaji;

import android.net.Uri;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.sebaya.bejaji.utils.GeneralHelper;

public class VideoFullScreenActivity extends AppCompatActivity {

    private VideoView videoView;
    public ImageView btnFullScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_full_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        videoView = (VideoView) findViewById(R.id.video_view);
        videoView.setVideoURI(Uri.parse(GeneralHelper.dataSapaan.url_video));
        videoView.setOnPreparedListener(() -> {
            videoView.seekTo(GeneralHelper.posisiVideo);
            videoView.start();
        });
        btnFullScreen = findViewById(R.id.btnFullScreen);
        btnFullScreen.setOnClickListener(view -> finish());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GeneralHelper.posisiVideo = videoView.getCurrentPosition();
    }
}