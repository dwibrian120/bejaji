package com.sebaya.bejaji;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VoiceAsistantActivity extends AppCompatActivity {

    EditText text,fromLangCode,toLangCode;
    TextView translatedText, btnJawa1, btnJawa2, btnIndo1, btnIndo2, btnInggris1, btnInggris2;
    TextToSpeech tts;
    CardView btnRecord, btnSpeaker, btnTerjemah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_asistant);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
            checkPermission();
        }

        previousStringList = new ArrayList<>();

        tts = new TextToSpeech(VoiceAsistantActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                tts.setLanguage(Locale.forLanguageTag("id"));
            }
        });

        initUi();

        btnTerjemah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                translate_api translate=new translate_api();
                translate.setOnTranslationCompleteListener(new translate_api.OnTranslationCompleteListener() {
                    @Override
                    public void onStartTranslation() {
                        // here you can perform initial work before translated the text like displaying progress bar
                    }

                    @Override
                    public void onCompleted(String text) {
                        // "text" variable will give you the translated text
                        translatedText.setText(text);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
                translate.execute(text.getText().toString(),fromLangCode.getText().toString(),toLangCode.getText().toString());
            }
        });

    }
    public static final Integer RecordAudioRequestCode = 1;
    private SpeechRecognizer speechRecognizer;
    private final int REQ_CODE_SPEECH_INPUT = 1;
    private List<String> previousStringList;
    @SuppressLint("ClickableViewAccessibility")
    private void promptSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.app_name));
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, 20000000);

        try {
            startActivityForResult(intent, RecordAudioRequestCode);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Your device is not supported",
                    Toast.LENGTH_SHORT).show();

            Log.d("br_bjj", a + "");
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO},RecordAudioRequestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RecordAudioRequestCode && grantResults.length > 0 ){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    final ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);


                    text.setText(result.get(0));
                    if (result.get(0) != null) {
                        previousStringList.add(result.get(0));
                    }
                }
                break;
            }

        }
    }

    private void initUi() {
        text=findViewById(R.id.text);
        fromLangCode=findViewById(R.id.from_lang);
        toLangCode=findViewById(R.id.to_lang);
        translatedText=findViewById(R.id.translated_text);
        btnTerjemah=findViewById(R.id.btnTerjemah);

        btnJawa1 = findViewById(R.id.btnJawa1);
        btnJawa2 = findViewById(R.id.btnJawa2);
        btnIndo1 = findViewById(R.id.btnIndo1);
        btnIndo2 = findViewById(R.id.btnIndo2);
        btnInggris1 = findViewById(R.id.btnInggris1);
        btnInggris2 = findViewById(R.id.btnInggris2);

        btnRecord = findViewById(R.id.btnRecord);
        btnSpeaker = findViewById(R.id.btnSpeaker);

        btnJawa1.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("id"));
            pilihBhs1(btnJawa1, "jw");
        });
        btnIndo1.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("id"));
            pilihBhs1(btnIndo1, "id");
        });
        btnInggris1.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("en"));
            pilihBhs1(btnInggris1, "en");
        });
        btnJawa2.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("id"));
            pilihBhs2(btnJawa2, "jw");
        });
        btnIndo2.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("id"));
            pilihBhs2(btnIndo2, "id");
        });
        btnInggris2.setOnClickListener(view -> {
            tts.setLanguage(Locale.forLanguageTag("en"));
            pilihBhs2(btnInggris2, "en");
        });

        btnRecord.setOnClickListener(view -> {
//            Toast.makeText(VoiceAsistantActivity.this, "Fitur dalam pengembangan!", Toast.LENGTH_SHORT).show();
            promptSpeechInput();
        });

        btnSpeaker.setOnClickListener(view -> {
            playTTS();
        });
    }

    private void playTTS(){
        tts.speak("" + translatedText.getText().toString().toString(), TextToSpeech.QUEUE_ADD, null);
    }

    private void pilihBhs1(TextView btnPilih, String kodeBhs){

        btnJawa1.setBackgroundColor(getResources().getColor(R.color.cocolate));
        btnIndo1.setBackgroundColor(getResources().getColor(R.color.cocolate));
        btnInggris1.setBackgroundColor(getResources().getColor(R.color.cocolate));

        btnPilih.setBackgroundColor(getResources().getColor(R.color.cocolate_muda));

        fromLangCode.setText(kodeBhs);
    }

    private void pilihBhs2(TextView btnPilih, String kodeBhs){

        btnJawa2.setBackgroundColor(getResources().getColor(R.color.cocolate));
        btnIndo2.setBackgroundColor(getResources().getColor(R.color.cocolate));
        btnInggris2.setBackgroundColor(getResources().getColor(R.color.cocolate));

        btnPilih.setBackgroundColor(getResources().getColor(R.color.cocolate_muda));

        toLangCode.setText(kodeBhs);
    }
}