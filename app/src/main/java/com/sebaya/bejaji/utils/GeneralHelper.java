package com.sebaya.bejaji.utils;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.widget.Toast;
import com.sebaya.bejaji.model.*;

import java.util.Calendar;

public class GeneralHelper {

    public static BannerModel dataBanner;
    public static SapaanModel dataSapaan;
    public static IptModel dataIpt;
    public static KategoriPanduanAktivitasModel dataKategoriPanduanAktivitas;
    public static PanduanAktivitasModel dataPanduanAktivitas;

    public static long posisiVideo = 0;

    public static String formatDate(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd MMM yyyy", cal).toString();
        return date;
    }

    public static void ToastNew(Context c, String message){
        Toast.makeText(c, "" + message, Toast.LENGTH_SHORT).show();
    }

    public void intentAct(Context c, Class cs){
        Intent u = new Intent(c, cs);
        c.startActivity(u);
    }

    public void intentActs(Context c, Class cs, String param){
        Intent u = new Intent(c, cs);
        u.putExtra("dataPut", param);
        c.startActivity(u);
    }

    //sjfkjfkjh

//    public static String formatBalance(String bal){
//
//        if(bal == null){
//            bal = "0";
//        }
//
//        String conBal = "";
//        String barArr[] = bal.split("");
//        String balDot = "";
//
//        int inde = 0;
//        Log.d("brbr", barArr.length + "");
//
//        for (int u = barArr.length-1; u >= 0; u--){
//            inde++;
//            if(inde%3 == 0){
//                if(u != 0) {
//                    if(inde == barArr.length-1){
//                        balDot = barArr[u] + balDot;
//                    }else {
//                        balDot = "." + barArr[u] + balDot;
//                    }
//                }else{
//                    balDot = barArr[u] + balDot;
//                }
//            }else {
//                balDot = barArr[u] + balDot;
//            }
//        }
//
//        Log.d("br_nom", bal);
//        Log.d("br_nom_com", balDot);
//
//        if(configSystem.format.equals("1")){
//            conBal = configSystem.currency + " " + bal;
//        }else if(configSystem.format.equals("2")){
//            conBal = configSystem.currency + " " + balDot;
//        }else if(configSystem.format.equals("3")){
//            conBal = bal + " " + configSystem.code;
//        }else if(configSystem.format.equals("4")){
//            conBal = balDot + " " + configSystem.code;
//        }else {
//            conBal = "-";
//        }
//        return conBal;
//    }

//    public void backPage(Activity act, View v){
//        ImageView iv = v.findViewById(R.id.btnBack);
//        iv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                act.finish();
//            }
//        });
//    }
}
