package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.adapter.PanduanAktivitasAdapter;
import com.sebaya.bejaji.model.PanduanAktivitasModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.sebaya.bejaji.utils.PopHelper;
import com.sebaya.bejaji.utils.PreferencesHelper;
import com.sebaya.bejaji.utils.UserHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DetailPanduanAktivitasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_panduan_aktivitas);
        ph = new PopHelper(this);

        getTitle = GeneralHelper.dataPanduanAktivitas.title;
        getDetail = GeneralHelper.dataPanduanAktivitas.description;
        getBanner = Config.BASE_URL + GeneralHelper.dataPanduanAktivitas.image;
        getDate = "# " + GeneralHelper.formatDate(Long.parseLong(GeneralHelper.dataPanduanAktivitas.create_at));

        _initView();

        backwardbtn = (ImageButton)findViewById(R.id.btnBackward);
        forwardbtn = (ImageButton)findViewById(R.id.btnForward);
        playbtn = (ImageButton)findViewById(R.id.btnPlay);
        pausebtn = (ImageButton)findViewById(R.id.btnPause);
        songName = (TextView)findViewById(R.id.txtSname);
        startTime = (TextView)findViewById(R.id.txtStartTime);
        songTime = (TextView)findViewById(R.id.txtSongTime);
        songName.setText("Baitikochi Chuste");

        String url = "" + GeneralHelper.dataPanduanAktivitas.url_audio;
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        if(url != null && !url.equals("")) {
            try {
                mPlayer.setDataSource(url);
                mPlayer.prepare(); // might take long! (for buffering, etc)
//            mPlayer.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        songPrgs = (SeekBar)findViewById(R.id.sBar);
        songPrgs.setClickable(false);
        pausebtn.setEnabled(false);

        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailPanduanAktivitasActivity.this, "Playing Audio", Toast.LENGTH_SHORT).show();
                mPlayer.start();
                eTime = mPlayer.getDuration();
                sTime = mPlayer.getCurrentPosition();
                if(oTime == 0){
                    songPrgs.setMax(eTime);
                    oTime =1;
                }
                songTime.setText(String.format("%d:%d", TimeUnit.MILLISECONDS.toMinutes(eTime),
                        TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS. toMinutes(eTime))) );
                startTime.setText(String.format("%d:%d", TimeUnit.MILLISECONDS.toMinutes(sTime),
                        TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS. toMinutes(sTime))) );
                songPrgs.setProgress(sTime);
                hdlr.postDelayed(UpdateSongTime, 100);
                pausebtn.setEnabled(true);
                playbtn.setEnabled(false);
            }
        });
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayer.pause();
                pausebtn.setEnabled(false);
                playbtn.setEnabled(true);
                Toast.makeText(getApplicationContext(),"Pausing Audio", Toast.LENGTH_SHORT).show();
            }
        });
        forwardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((sTime + fTime) <= eTime)
                {
                    sTime = sTime + fTime;
                    mPlayer.seekTo(sTime);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Cannot jump forward 5 seconds", Toast.LENGTH_SHORT).show();
                }
                if(!playbtn.isEnabled()){
                    playbtn.setEnabled(true);
                }
            }
        });
        backwardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((sTime - bTime) > 0)
                {
                    sTime = sTime - bTime;
                    mPlayer.seekTo(sTime);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Cannot jump backward 5 seconds", Toast.LENGTH_SHORT).show();
                }
                if(!playbtn.isEnabled()){
                    playbtn.setEnabled(true);
                }
            }
        });
    }

    public PopHelper ph;
    public String getTitle, getDetail, getBanner, getDate, getLike;

    private ImageButton forwardbtn, backwardbtn, pausebtn, playbtn;
    private MediaPlayer mPlayer;
    private TextView songName, startTime, songTime;
    private SeekBar songPrgs;
    private static int oTime =0, sTime =0, eTime =0, fTime = 5000, bTime = 5000;
    private Handler hdlr = new Handler();

    private Runnable UpdateSongTime = new Runnable() {
        @Override
        public void run() {
            sTime = mPlayer.getCurrentPosition();
            startTime.setText(String.format("%d:%d", TimeUnit.MILLISECONDS.toMinutes(sTime),
                    TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))) );
            songPrgs.setProgress(sTime);
            hdlr.postDelayed(this, 100);
        }
    };

    public TextView txtTitle, txtDate, txtLike;
    public ImageView imgBanner, btnLike;
    public WebView webDetail;

    public void _initView(){
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        txtTitle = findViewById(R.id.txtTitle);
        txtDate = findViewById(R.id.txtDate);
        imgBanner = findViewById(R.id.imgBanner);
        webDetail = findViewById(R.id.webDetail);
        btnLike = findViewById(R.id.btnLike);
        txtLike = findViewById(R.id.txtLike);

        _actionView();
    }

    public void _actionView(){
        txtTitle.setText("" + getTitle);
        txtDate.setText("" + getDate);
        txtLike.setText("" + GeneralHelper.dataPanduanAktivitas.jumlah_like);
        displayDetail(getDetail);
        Glide.with(this)
                .load(getBanner)
                .apply(new RequestOptions().placeholder(R.drawable.paypay_icon).error(R.drawable.paypay_icon))
                .into(imgBanner);

        Log.d("br_cek", GeneralHelper.dataPanduanAktivitas.id_kategori);


        getLike = GeneralHelper.dataPanduanAktivitas.id_like;

        setLike();

        btnLike.setOnClickListener(view -> {
            reqLikePanduanAktivitas();
        });
        reqPanduanAktivitas();
    }

    @SuppressLint("CheckResult")
    public void reqPanduanAktivitas(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqPanduanAktivitas(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        GeneralHelper.dataPanduanAktivitas.id_kategori + "",
                        PreferencesHelper.getKeyLanguage(DetailPanduanAktivitasActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVPanduanAktivitas(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVPanduanAktivitas(ArrayList<PanduanAktivitasModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvPanduanAktivitas);
        rvDataView.setLayoutManager(layoutManager);

        PanduanAktivitasAdapter viewAdapter = new PanduanAktivitasAdapter(respData, DetailPanduanAktivitasActivity.this, Integer.parseInt(GeneralHelper.dataPanduanAktivitas.id));
        rvDataView.setAdapter(viewAdapter);
    }

    public void setLike(){
        if(getLike == null){
            btnLike.setColorFilter(ContextCompat.getColor(DetailPanduanAktivitasActivity.this, R.color.text_app), android.graphics.PorterDuff.Mode.SRC_IN);
        }else {
            btnLike.setColorFilter(ContextCompat.getColor(DetailPanduanAktivitasActivity.this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @SuppressLint("CheckResult")
    public void reqLikePanduanAktivitas(){
        ph.popLoading();
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqLikePanduanAktivitas(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        GeneralHelper.dataPanduanAktivitas.id + "",
                        PreferencesHelper.getKeyLanguage(DetailPanduanAktivitasActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            ph.popDismiss();
                            if(respData.response){
                                getLike = respData.data.id_like;
                                txtLike.setText(respData.data.jumlah_like);
                                setLike();
                            }
                            Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                        },
                        throwable -> {
                            ph.popDismiss();
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayDetail(String detail) {
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += detail;
        webDetail.getSettings().setJavaScriptEnabled(true);
        webDetail.getSettings();
        webDetail.getSettings().setBuiltInZoomControls(true);
        webDetail.setBackgroundColor(Color.TRANSPARENT);
        webDetail.setWebChromeClient(new WebChromeClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            webDetail.loadDataWithBaseURL(null, html_data, "text/html; charset=UTF-8", "utf-8", null);
        } else {
            webDetail.loadData(html_data, "text/html; charset=UTF-8", null);
        }
        // disable scroll on touch
        webDetail.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webDetail.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + url));
                startActivity(browserIntent);
                return true;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayer.stop();
    }
}