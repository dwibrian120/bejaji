package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.sebaya.bejaji.adapter.SapaanAdapter;
import com.sebaya.bejaji.model.SapaanModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.*;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class DetailSapaanActivity extends AppCompatActivity {

    public PopHelper ph;
    public String getTitle, getDetail, getDate, getUrlVideo, getLike;

    public TextView txtTitle, txtDate, txtLike;
    public WebView webDetail;
    public ImageView btnFullScreen, btnLike;

    public VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_sapaan);
        ph = new PopHelper(this);

        getTitle = GeneralHelper.dataSapaan.title;
        getDetail = GeneralHelper.dataSapaan.description;
        getUrlVideo = GeneralHelper.dataSapaan.url_video;
        getLike = GeneralHelper.dataSapaan.id_like;
        getDate = "# " + GeneralHelper.formatDate(Long.parseLong(GeneralHelper.dataSapaan.create_at));

        _initView();
    }

    public void _initView(){
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        txtTitle = findViewById(R.id.txtTitle);
        txtDate = findViewById(R.id.txtDate);
        webDetail = findViewById(R.id.webDetail);
        btnFullScreen = findViewById(R.id.btnFullScreen);
        videoView = findViewById(R.id.video_view);
        btnLike = findViewById(R.id.btnLike);
        txtLike = findViewById(R.id.txtLike);

        _actionView();
        playVideo();
    }

    public void _actionView(){
        txtTitle.setText("" + getTitle);
        txtDate.setText("" + getDate);
        txtLike.setText("" + GeneralHelper.dataSapaan.jumlah_like);
        displayDetail(getDetail);

        setLike();

        btnLike.setOnClickListener(view -> {
            reqLikeSapaan();
        });

        btnFullScreen.setOnClickListener(view -> {
            videoView.stopPlayback();
            GeneralHelper.posisiVideo = videoView.getCurrentPosition();
            startActivity(new Intent(DetailSapaanActivity.this, VideoFullScreenActivity.class));
        });
        reqSapaan();
    }

    @SuppressLint("CheckResult")
    public void reqSapaan(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqSapaan(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        PreferencesHelper.getKeyLanguage(DetailSapaanActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVSapaan(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVSapaan(ArrayList<SapaanModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvSapaan);
        rvDataView.setLayoutManager(layoutManager);

        SapaanAdapter viewAdapter = new SapaanAdapter(respData, DetailSapaanActivity.this, Integer.parseInt(GeneralHelper.dataSapaan.id));
        rvDataView.setAdapter(viewAdapter);
    }

    public void setLike(){
        if(getLike == null){
            btnLike.setColorFilter(ContextCompat.getColor(DetailSapaanActivity.this, R.color.text_app), android.graphics.PorterDuff.Mode.SRC_IN);
        }else {
            btnLike.setColorFilter(ContextCompat.getColor(DetailSapaanActivity.this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @SuppressLint("CheckResult")
    public void reqLikeSapaan(){
        ph.popLoading();
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqLikeSapaan(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        GeneralHelper.dataSapaan.id + "",
                        PreferencesHelper.getKeyLanguage(DetailSapaanActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            ph.popDismiss();
                            if(respData.response){
                                getLike = respData.data.id_like;
                                txtLike.setText(respData.data.jumlah_like);
                                setLike();
                            }
                            Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                        },
                        throwable -> {
                            ph.popDismiss();
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void playVideo(){
        videoView.setVideoURI(Uri.parse(getUrlVideo));
        videoView.setOnPreparedListener(() -> {
            if(GeneralHelper.posisiVideo != 0){
                videoView.seekTo(GeneralHelper.posisiVideo);
            }
            videoView.start();
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayDetail(String detail) {
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += detail;
        webDetail.getSettings().setJavaScriptEnabled(true);
        webDetail.getSettings();
        webDetail.getSettings().setBuiltInZoomControls(true);
        webDetail.setBackgroundColor(Color.TRANSPARENT);
        webDetail.setWebChromeClient(new WebChromeClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            webDetail.loadDataWithBaseURL(null, html_data, "text/html; charset=UTF-8", "utf-8", null);
        } else {
            webDetail.loadData(html_data, "text/html; charset=UTF-8", null);
        }
        // disable scroll on touch
        webDetail.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webDetail.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + url));
                startActivity(browserIntent);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        playVideo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GeneralHelper.posisiVideo = 0;
    }
}