package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.sebaya.bejaji.adapter.PanduanAktivitasAdapter;
import com.sebaya.bejaji.adapter.SapaanAdapter;
import com.sebaya.bejaji.model.PanduanAktivitasModel;
import com.sebaya.bejaji.model.SapaanModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.sebaya.bejaji.utils.PreferencesHelper;
import com.sebaya.bejaji.utils.UserHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import org.w3c.dom.Text;
import retrofit2.Retrofit;

import java.util.ArrayList;

public class PanduanAktivitasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panduan_aktivitas);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());

        TextView txtTitle = findViewById(R.id.txtTitle);
        txtTitle.setText(GeneralHelper.dataKategoriPanduanAktivitas.title);

        reqPanduanAktivitas();
    }

    @SuppressLint("CheckResult")
    public void reqPanduanAktivitas(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqPanduanAktivitas(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        GeneralHelper.dataKategoriPanduanAktivitas.id + "",
                        PreferencesHelper.getKeyLanguage(PanduanAktivitasActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVPanduanAktivitas(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVPanduanAktivitas(ArrayList<PanduanAktivitasModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvPanduanAktivitas);
        rvDataView.setLayoutManager(layoutManager);

        PanduanAktivitasAdapter viewAdapter = new PanduanAktivitasAdapter(respData, PanduanAktivitasActivity.this, 0);
        rvDataView.setAdapter(viewAdapter);
    }
}