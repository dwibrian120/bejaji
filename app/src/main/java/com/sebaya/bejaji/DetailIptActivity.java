package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.adapter.IptAdapter;
import com.sebaya.bejaji.model.IptModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.sebaya.bejaji.utils.PopHelper;
import com.sebaya.bejaji.utils.PreferencesHelper;
import com.sebaya.bejaji.utils.UserHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DetailIptActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ipt);
        ph = new PopHelper(this);

        getTitle = GeneralHelper.dataIpt.title;
        getDetail = GeneralHelper.dataIpt.description;
        getBanner = Config.BASE_URL + GeneralHelper.dataIpt.image;
        getDate = "# " + GeneralHelper.formatDate(Long.parseLong(GeneralHelper.dataIpt.create_at));

        _initView();
    }

    public PopHelper ph;
    public String getTitle, getDetail, getBanner, getDate, getLike;

    public TextView txtTitle, txtDate, txtLike, txtAlamat;
    public ImageView imgBanner, btnLike;
    public WebView webDetail;
    public CardView btnMaps;

    public void _initView(){
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        txtTitle = findViewById(R.id.txtTitle);
        txtDate = findViewById(R.id.txtDate);
        imgBanner = findViewById(R.id.imgBanner);
        webDetail = findViewById(R.id.webDetail);
        btnLike = findViewById(R.id.btnLike);
        txtLike = findViewById(R.id.txtLike);
        btnMaps = findViewById(R.id.btnMaps);
        txtAlamat = findViewById(R.id.txtAlamat);

        _actionView();
    }

    public void _actionView(){
        txtTitle.setText("" + getTitle);
        txtDate.setText("" + getDate);
        txtAlamat.setText("" + GeneralHelper.dataIpt.alamat);
        txtLike.setText("" + GeneralHelper.dataIpt.jumlah_like);
        displayDetail(getDetail);
        Glide.with(this)
                .load(getBanner)
                .apply(new RequestOptions().placeholder(R.drawable.paypay_icon).error(R.drawable.paypay_icon))
                .into(imgBanner);

        getLike = GeneralHelper.dataIpt.id_like;

        setLike();

        btnLike.setOnClickListener(view -> {
            reqLikeIpt();
        });

        btnMaps.setOnClickListener(view -> {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + GeneralHelper.dataIpt.url_maps));
                startActivity(browserIntent);
            }catch (Exception e){
                Toast.makeText(DetailIptActivity.this, "Terjadi kesalahan. Coba lagi nanti!", Toast.LENGTH_SHORT).show();
            }
        });
        reqIpt();
    }

    @SuppressLint("CheckResult")
    public void reqIpt(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqIpt(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        PreferencesHelper.getKeyLanguage(DetailIptActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVIpt(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVIpt(ArrayList<IptModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvIpt);
        rvDataView.setLayoutManager(layoutManager);

        IptAdapter viewAdapter = new IptAdapter(respData, DetailIptActivity.this, Integer.parseInt(GeneralHelper.dataIpt.id));
        rvDataView.setAdapter(viewAdapter);
    }

    public void setLike(){
        if(getLike == null){
            btnLike.setColorFilter(ContextCompat.getColor(DetailIptActivity.this, R.color.text_app), android.graphics.PorterDuff.Mode.SRC_IN);
        }else {
            btnLike.setColorFilter(ContextCompat.getColor(DetailIptActivity.this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @SuppressLint("CheckResult")
    public void reqLikeIpt(){
        ph.popLoading();
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqLikeIpt(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        GeneralHelper.dataIpt.id + "",
                        PreferencesHelper.getKeyLanguage(DetailIptActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            ph.popDismiss();
                            if(respData.response){
                                getLike = respData.data.id_like;
                                txtLike.setText(respData.data.jumlah_like);
                                setLike();
                            }
                            Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                        },
                        throwable -> {
                            ph.popDismiss();
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayDetail(String detail) {
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += detail;
        webDetail.getSettings().setJavaScriptEnabled(true);
        webDetail.getSettings();
        webDetail.getSettings().setBuiltInZoomControls(true);
        webDetail.setBackgroundColor(Color.TRANSPARENT);
        webDetail.setWebChromeClient(new WebChromeClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            webDetail.loadDataWithBaseURL(null, html_data, "text/html; charset=UTF-8", "utf-8", null);
        } else {
            webDetail.loadData(html_data, "text/html; charset=UTF-8", null);
        }
        // disable scroll on touch
        webDetail.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webDetail.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + url));
                startActivity(browserIntent);
                return true;
            }
        });
    }
}