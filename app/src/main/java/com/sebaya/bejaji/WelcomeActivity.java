package com.sebaya.bejaji;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import com.sebaya.bejaji.utils.LocaleHelper;
import com.sebaya.bejaji.utils.PreferencesHelper;
import org.intellij.lang.annotations.Language;

import java.util.Locale;

public class WelcomeActivity extends AppCompatActivity {

    public CardView btnIndo, btnEnglish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocale(PreferencesHelper.getKeyLanguage(WelcomeActivity.this));
        setContentView(R.layout.activity_welcome);
//        if(PreferencesHelper.getKeyLanguage(WelcomeActivity.this) != null){
//            if(!PreferencesHelper.getKeyLanguage(WelcomeActivity.this).equals("")){
//                startActivity(new Intent(this, LoginActivity.class));
//                finish();
//            }
//        }

        initInisView();
        initActView();
    }

    public void initInisView(){
        btnIndo = findViewById(R.id.btnIndo);
        btnEnglish = findViewById(R.id.btnEnglish);
    }

    public void initActView(){
        btnIndo.setOnClickListener(view -> {
            setLocale("in");
            startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            finish();
        });

        btnEnglish.setOnClickListener(view -> {
            setLocale("en");
            startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            finish();
        });
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        PreferencesHelper.setKeyLanguage(WelcomeActivity.this, lang);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}