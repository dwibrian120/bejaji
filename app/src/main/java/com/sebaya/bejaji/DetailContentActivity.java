/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.sebaya.bejaji.utils.PopHelper;

public class DetailContentActivity extends AppCompatActivity {

    public PopHelper ph;
    public String getTitle, getDetail, getBanner, getDate, getUrlVideo;

    //sd kjshf kjsfhk jshfkj sjfh ksjfh kjfdfgdf

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_content);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        ph = new PopHelper(this);

        getTitle = GeneralHelper.dataBanner.title;
        getDetail = GeneralHelper.dataBanner.description;
        getBanner = Config.BASE_URL + GeneralHelper.dataBanner.image;
        getDate = "# " + GeneralHelper.formatDate(Long.parseLong(GeneralHelper.dataBanner.create_at));

        _initView();
    }

    public TextView txtTitle, txtDate;
    public ImageView imgBanner;
    public WebView webDetail;

    public void _initView(){
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());
        txtTitle = findViewById(R.id.txtTitle);
        txtDate = findViewById(R.id.txtDate);
        imgBanner = findViewById(R.id.imgBanner);
        webDetail = findViewById(R.id.webDetail);

        _actionView();
    }

    public void _actionView(){
        txtTitle.setText("" + getTitle);
        txtDate.setText("" + getDate);
        displayDetail(getDetail);
        Glide.with(this)
                .load(getBanner)
                .apply(new RequestOptions().placeholder(R.drawable.paypay_icon).error(R.drawable.paypay_icon))
                .into(imgBanner);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void displayDetail(String detail) {
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += detail;
        webDetail.getSettings().setJavaScriptEnabled(true);
        webDetail.getSettings();
        webDetail.getSettings().setBuiltInZoomControls(true);
        webDetail.setBackgroundColor(Color.TRANSPARENT);
        webDetail.setWebChromeClient(new WebChromeClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            webDetail.loadDataWithBaseURL(null, html_data, "text/html; charset=UTF-8", "utf-8", null);
        } else {
            webDetail.loadData(html_data, "text/html; charset=UTF-8", null);
        }
        // disable scroll on touch
        webDetail.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        // override url direct
        webDetail.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" + url));
                startActivity(browserIntent);
                return true;
            }
        });
    }
}