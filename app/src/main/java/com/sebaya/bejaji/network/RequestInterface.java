package com.sebaya.bejaji.network;

import com.sebaya.bejaji.model.*;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RequestInterface {

    @FormUrlEncoded
    @POST("login")
    Observable<DataModel<LoginModel>> reqLogin(
            @Field("codeApp") String codeApp,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("register")
    Observable<DataModel<LoginModel>> reqRegister(
            @Field("codeApp") String codeApp,
            @Field("email") String email,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("passconf") String passconf
    );

    @FormUrlEncoded
    @POST("req_code_reset_password")
    Observable<DataModel<ForgotPasswordModel>> reqForgot(
            @Field("codeApp") String codeApp,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("reset_password")
    Observable<DataModel<LoginModel>> reqResetPassword(
            @Field("codeApp") String codeApp,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("update_profile")
    Observable<DataModel<LoginModel>> reqUpdateProfile(
            @Field("codeApp") String codeApp,
            @Field("name") String name,
            @Field("token") String token,
            @Field("id") String id,
            @Field("phone") String phone);

    @Multipart
    @POST("update_avatar")
    Observable<DataModel<LoginModel>> reqUpdateAvatar(
            @Part("codeApp") RequestBody codeApp,
            @Part("id") RequestBody id,
            @Part("token") RequestBody token,
            @Part MultipartBody.Part avatar);

    @Multipart
    @POST("update_id_card")
    Observable<DataModel<LoginModel>> reqUpdateIdCard(
            @Part("codeApp") RequestBody codeApp,
            @Part("id") RequestBody id,
            @Part("token") RequestBody token,
            @Part MultipartBody.Part id_card);

    @FormUrlEncoded
    @POST("banner")
    Observable<DataListModel<BannerModel>> reqBanner(
            @Field("codeApp") String codeApp,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("ipt")
    Observable<DataListModel<IptModel>> reqIpt(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("sapaan")
    Observable<DataListModel<SapaanModel>> reqSapaan(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("panduan_aktivitas")
    Observable<DataListModel<PanduanAktivitasModel>> reqPanduanAktivitas(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("id_kategori") String id_kategori,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("panduan_aktivitas_kategori")
    Observable<DataListModel<KategoriPanduanAktivitasModel>> reqKategoriPanduanAktivitas(
            @Field("codeApp") String codeApp,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("like_sapaan")
    Observable<DataModel<SapaanModel>> reqLikeSapaan(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("id_sapaan") String id_sapaan,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("like_panduan_aktivitas")
    Observable<DataModel<SapaanModel>> reqLikePanduanAktivitas(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("id_panduan_aktivitas") String id_panduan_aktivitas,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("like_ipt")
    Observable<DataModel<IptModel>> reqLikeIpt(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("id_ipt") String id_ipt,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("konfigurasi")
    Observable<DataModel<KonfigurasiModel>> reqKonfigurasi(
            @Field("codeApp") String codeApp,
            @Field("bahasa") String bahasa
    );

    @FormUrlEncoded
    @POST("favorit")
    Observable<DataModel<FavoritModel>> reqFavorit(
            @Field("codeApp") String codeApp,
            @Field("id_pengguna") String id_pengguna,
            @Field("bahasa") String bahasa
    );
}
