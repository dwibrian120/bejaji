package com.sebaya.bejaji.network;

public class Config {
    public static String BASE_URL = "https://bejaji.sebaya.id/";
    public static String BASE_API = BASE_URL + "api/";

    //Path Image
    public static String PATH_IMG = BASE_URL + "assets/vendor/images/";
    public static String CODE_APP = "123456";
}
