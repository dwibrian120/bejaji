/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

public class PanduanAktivitasModel {
    public String id;
    public String id_kategori;
    public String title;
    public String title_en;
    public String image;
    public String image_en;
    public String description;
    public String description_en;
    public String url_audio;
    public String url_audio_en;
    public String id_like;
    public String jumlah_like;
    public String create_at;
}
