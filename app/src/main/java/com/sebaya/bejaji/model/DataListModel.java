package com.sebaya.bejaji.model;

import java.util.ArrayList;

public class DataListModel<T> {
    public boolean response;
    public String message;
    public ArrayList<T> data;
}
