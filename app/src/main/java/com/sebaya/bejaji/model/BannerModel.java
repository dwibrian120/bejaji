/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

public class BannerModel {
    public String id;
    public String title;
    public String title_en;
    public String image;
    public String image_en;
    public String description;
    public String description_en;
    public String create_at;
}
