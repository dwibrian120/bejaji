/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

import java.util.ArrayList;

public class FavoritModel {
    public ArrayList<SapaanModel> sapaan;
    public ArrayList<PanduanAktivitasModel> panduan_aktivitas;
    public ArrayList<IptModel> ipt;
}
