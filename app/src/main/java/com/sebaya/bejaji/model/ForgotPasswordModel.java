/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

public class ForgotPasswordModel {
    public String id;
    public String email;
    public String code;
    public String expired_time;
}
