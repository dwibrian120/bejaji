/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

public class NewsModel {
    public String id;
    public String title;
    public String image;
    public String description;
    public String status;
    public String create_at;
}
