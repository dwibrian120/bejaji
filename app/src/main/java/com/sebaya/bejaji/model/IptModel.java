/*
 * Create by Brian Dwi Murdianto
 * Email : dwibrian120@gmail.com
 */

package com.sebaya.bejaji.model;

public class IptModel {
    public String id;
    public String title;
    public String title_en;
    public String image;
    public String image_en;
    public String description;
    public String description_en;
    public String alamat;
    public String alamat_en;
    public String url_maps;
    public String url_maps_en;
    public String id_like;
    public String jumlah_like;
    public String create_at;
}
