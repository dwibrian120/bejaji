package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
//import com.sebaya.bejaji.adapter.MapelAdapter;
import com.sebaya.bejaji.adapter.ImageSlideAdapter;
import com.sebaya.bejaji.adapter.NewsAdapter;
import com.sebaya.bejaji.model.BannerModel;
import com.sebaya.bejaji.model.NewsModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.FlagHelper;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.sebaya.bejaji.utils.PopHelper;
import com.sebaya.bejaji.utils.PreferencesHelper;
import com.sebaya.bejaji.utils.UserHelper;

import java.util.ArrayList;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    public PopHelper ph;
    public GeneralHelper gh;
    public ImageView imgProfile;
    public TextView txtNama;
    public LinearLayout btnBottomHome, btnBottomProfile, btnBottomFavorit, layBanner, btnSapaan, btnPanduanAktivitas, btnIpt, btnAsistenSuara;
//    public ArrayList<KelasModel> km;
    public String idKelas = "", namaKelas = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ph = new PopHelper(this);
        gh = new GeneralHelper();
        _initView();
        reqBanner();
    }

    public void _initView(){
        btnBottomHome = findViewById(R.id.btnBottomHome);
        btnBottomProfile = findViewById(R.id.btnBottomProfile);
        btnBottomFavorit = findViewById(R.id.btnBottomFavorit);
        imgProfile = findViewById(R.id.imgProfile);
        txtNama = findViewById(R.id.txtNama);
        layBanner = findViewById(R.id.layBanner);

        btnSapaan = findViewById(R.id.btnSapaan);
        btnPanduanAktivitas = findViewById(R.id.btnPanduanAktivitas);
        btnIpt = findViewById(R.id.btnIpt);
        btnAsistenSuara = findViewById(R.id.btnAsistenSuara);

        _actionView();
    }

    public void _actionView(){

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        layBanner.getLayoutParams().height = width/2;

        Glide.with(this)
                .load(UserHelper.user.avatar)
                .apply(new RequestOptions().placeholder(R.color.white).error(R.color.white).circleCrop())
                .apply(new RequestOptions().circleCrop())
                .into(imgProfile);

        txtNama.setText("" + UserHelper.user.name);

        btnBottomProfile.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, ProfileActivity.class)));
        btnAsistenSuara.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, VoiceAsistantActivity.class)));
        btnSapaan.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, SapaanActivity.class)));
        btnPanduanAktivitas.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, KategoriPanduanAktivitasActivity.class)));
        btnIpt.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, IptActivity.class)));
        btnBottomFavorit.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, FavoritActivity.class)));
        btnBottomHome.setOnClickListener(v -> {startActivity(new Intent(MainActivity.this, MainActivity.class)); finish();});
    }

    @SuppressLint("CheckResult")
    public void reqBanner(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqBanner(
                        Config.CODE_APP + "",
                        PreferencesHelper.getKeyLanguage(MainActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVBanner(respData.data);
                                initRVNews(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }


    public void initRVBanner(ArrayList<BannerModel> respData) {
        SliderView sliderView = findViewById(R.id.imageSlider);

        ImageSlideAdapter adapter = new ImageSlideAdapter(this, respData);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
    }

    public void initRVNews(ArrayList<BannerModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvBerita);
        rvDataView.setLayoutManager(layoutManager);

        NewsAdapter viewAdapter = new NewsAdapter(respData, MainActivity.this);
        rvDataView.setAdapter(viewAdapter);
    }

    @Override
    public void onResume(){
        FlagHelper.pos = 0;
        _initView();
        super.onResume();
    }
}