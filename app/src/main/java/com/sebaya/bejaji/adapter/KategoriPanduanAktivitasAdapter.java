package com.sebaya.bejaji.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.DetailSapaanActivity;
import com.sebaya.bejaji.PanduanAktivitasActivity;
import com.sebaya.bejaji.R;
import com.sebaya.bejaji.model.KategoriPanduanAktivitasModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.utils.GeneralHelper;

import java.util.ArrayList;

public class KategoriPanduanAktivitasAdapter extends RecyclerView.Adapter<KategoriPanduanAktivitasAdapter.ViewHolder>{

    public ArrayList<KategoriPanduanAktivitasModel> rvData;
    public Context context;

    public KategoriPanduanAktivitasAdapter(ArrayList<KategoriPanduanAktivitasModel> inputData, Context context) {
        this.rvData = inputData;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgNews;
        public TextView txtTitle;
        public CardView itemNews;

        public ViewHolder(View v) {
            super(v);
            imgNews = v.findViewById(R.id.imgNews);
            txtTitle = v.findViewById(R.id.txtTitle);
            itemNews = v.findViewById(R.id.itemNews);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_kategori_panduan_aktivitas, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtTitle.setText(rvData.get(position).title);
        Glide.with(context)
                .load(Config.BASE_URL + rvData.get(position).image)
                .apply(new RequestOptions().placeholder(R.drawable.logo_duta_bahasa).error(R.drawable.logo_duta_bahasa))
                .into(holder.imgNews);
        holder.itemNews.setOnClickListener(view -> {
            Intent u = new Intent(context, PanduanAktivitasActivity.class);
            GeneralHelper.dataKategoriPanduanAktivitas = rvData.get(position);
            context.startActivity(u);
        });
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }
}
