package com.sebaya.bejaji.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.sebaya.bejaji.DetailContentActivity;
import com.sebaya.bejaji.R;
import com.sebaya.bejaji.model.BannerModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.utils.GeneralHelper;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class ImageSlideAdapter extends
        SliderViewAdapter<ImageSlideAdapter.SliderAdapterVH> {

    private Context context;
    private ArrayList<BannerModel> mSliderItems;

    public ImageSlideAdapter(Context context, ArrayList<BannerModel> sliderItems) {
        this.mSliderItems = sliderItems;
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_image_slide, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        BannerModel sliderItem = mSliderItems.get(position);

        viewHolder.textViewDescription.setText(sliderItem.title);
        viewHolder.textViewDescription.setTextSize(14);
        viewHolder.textViewDescription.setTextColor(Color.WHITE);
        Glide.with(viewHolder.itemView)
                .load(Config.BASE_URL + "" + sliderItem.image)
                .into(viewHolder.imageViewBackground);

        viewHolder.itemView.setOnClickListener(v -> {
            Intent u = new Intent(context, DetailContentActivity.class);
            GeneralHelper.dataBanner = mSliderItems.get(position);
            context.startActivity(u);
        });
    }

    @Override
    public int getCount() {
        return mSliderItems != null ? mSliderItems.size() < 3 ? mSliderItems.size() : 3 : 0;
    }

    class SliderAdapterVH extends ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }

}