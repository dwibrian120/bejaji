package com.sebaya.bejaji.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sebaya.bejaji.DetailPanduanAktivitasActivity;
import com.sebaya.bejaji.DetailSapaanActivity;
import com.sebaya.bejaji.PanduanAktivitasActivity;
import com.sebaya.bejaji.R;
import com.sebaya.bejaji.model.PanduanAktivitasModel;
import com.sebaya.bejaji.model.SapaanModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.utils.GeneralHelper;

import java.util.ArrayList;

public class PanduanAktivitasAdapter extends RecyclerView.Adapter<PanduanAktivitasAdapter.ViewHolder>{

    public ArrayList<PanduanAktivitasModel> rvData;
    public Context context;
    public int ind;

    public PanduanAktivitasAdapter(ArrayList<PanduanAktivitasModel> inputData, Context context, int ind) {
        this.rvData = inputData;
        this.context = context;
        this.ind = ind;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgNews;
        public TextView txtTitle, txtDate, txtLike, txtDescription;
        public CardView itemNews;

        public ViewHolder(View v) {
            super(v);
            imgNews = v.findViewById(R.id.imgNews);
            txtTitle = v.findViewById(R.id.txtTitle);
            txtDate = v.findViewById(R.id.txtDate);
            txtLike = v.findViewById(R.id.txtLike);
            txtDescription = v.findViewById(R.id.txtDescription);
            itemNews = v.findViewById(R.id.itemNews);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_panduan_aktivitas, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtTitle.setText(rvData.get(position).title);
        holder.txtLike.setText(rvData.get(position).jumlah_like);
        holder.txtDescription.setText(Html.fromHtml(rvData.get(position).description));
        holder.txtDate.setText("# " + GeneralHelper.formatDate(Long.parseLong(rvData.get(position).create_at)));
        Glide.with(context)
                .load(Config.BASE_URL + rvData.get(position).image)
                .apply(new RequestOptions().placeholder(R.drawable.logo_duta_bahasa).error(R.drawable.logo_duta_bahasa))
                .into(holder.imgNews);

        holder.itemNews.setOnClickListener(view -> {
            Intent u = new Intent(context, DetailPanduanAktivitasActivity.class);
            GeneralHelper.dataPanduanAktivitas = rvData.get(position);
            context.startActivity(u);
        });

        if(ind == Integer.parseInt(rvData.get(position).id)){
            holder.itemNews.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }
}
