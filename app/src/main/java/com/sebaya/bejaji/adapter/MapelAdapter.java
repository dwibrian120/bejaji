//package com.sebaya.bejaji.adapter;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.sebaya.bejaji.R;
//
//import java.util.ArrayList;
//
//public class MapelAdapter extends RecyclerView.Adapter<MapelAdapter.ViewHolder>{
//
//    public ArrayList<MapelModel> rvData;
//    public Context context;
//
//    public MapelAdapter(ArrayList<MapelModel> inputData, Context context) {
//        this.rvData = inputData;
//        this.context = context;
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView txtNama;
//        public LinearLayout btnItem;
//
//        public ViewHolder(View v) {
//            super(v);
//            txtNama = v.findViewById(R.id.txtNama);
//            btnItem = v.findViewById(R.id.btnItem);
//        }
//    }
//
//    @NonNull
//    @Override
//    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        // membuat view baru
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_mapel, parent, false);
//        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
//        ViewHolder vh = new ViewHolder(v);
//        return vh;
//    }
//
//    @SuppressLint("ResourceAsColor")
//    @Override
//    public void onBindViewHolder(ViewHolder holder, final int position) {
//        holder.txtNama.setText(rvData.get(position).nama);
//
//        holder.btnItem.setOnClickListener(view -> {
//            Intent i = new Intent(context, PertemuanActivity.class);
//            i.putExtra("idMapel", rvData.get(position).id);
//            i.putExtra("namaMapel", rvData.get(position).nama);
//            context.startActivity(i);
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return rvData != null ? rvData.size() : 0;
//    }
//}
