package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.sebaya.bejaji.adapter.NewsAdapter;
import com.sebaya.bejaji.adapter.SapaanAdapter;
import com.sebaya.bejaji.model.BannerModel;
import com.sebaya.bejaji.model.SapaanModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.PreferencesHelper;
import com.sebaya.bejaji.utils.UserHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import java.util.ArrayList;

public class SapaanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sapaan);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());

        reqSapaan();
    }

    @SuppressLint("CheckResult")
    public void reqSapaan(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqSapaan(
                        Config.CODE_APP + "",
                        UserHelper.user.id + "",
                        PreferencesHelper.getKeyLanguage(SapaanActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVSapaan(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVSapaan(ArrayList<SapaanModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvSapaan);
        rvDataView.setLayoutManager(layoutManager);

        SapaanAdapter viewAdapter = new SapaanAdapter(respData, SapaanActivity.this, 0);
        rvDataView.setAdapter(viewAdapter);
    }
}