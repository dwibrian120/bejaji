package com.sebaya.bejaji;

import android.annotation.SuppressLint;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.sebaya.bejaji.adapter.KategoriPanduanAktivitasAdapter;
import com.sebaya.bejaji.adapter.SapaanAdapter;
import com.sebaya.bejaji.model.KategoriPanduanAktivitasModel;
import com.sebaya.bejaji.network.Config;
import com.sebaya.bejaji.network.RequestInterface;
import com.sebaya.bejaji.network.RetrofitClient;
import com.sebaya.bejaji.utils.PreferencesHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import java.util.ArrayList;

public class KategoriPanduanAktivitasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori_panduan_aktivitas);
        findViewById(R.id.btnBack).setOnClickListener(view -> finish());

        reqKategoriPanduanAktivitas();
    }

    @SuppressLint("CheckResult")
    public void reqKategoriPanduanAktivitas(){
        Retrofit retrofit = RetrofitClient.getClient(this);
        RequestInterface request = retrofit.create(RequestInterface.class);
        request.reqKategoriPanduanAktivitas(
                        Config.CODE_APP + "",
                        PreferencesHelper.getKeyLanguage(KategoriPanduanAktivitasActivity.this) + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        respData -> {
                            if(respData.response){
                                initRVKategoriPanduanAktivitas(respData.data);
                            }else{
                                Toast.makeText(getApplicationContext(), respData.message, Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> {
                            Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                );
    }

    public void initRVKategoriPanduanAktivitas(ArrayList<KategoriPanduanAktivitasModel> respData) {
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecyclerView rvDataView = findViewById(R.id.rvKategoriPanduanAktivitas);
        rvDataView.setLayoutManager(layoutManager);

        KategoriPanduanAktivitasAdapter viewAdapter = new KategoriPanduanAktivitasAdapter(respData, KategoriPanduanAktivitasActivity.this);
        rvDataView.setAdapter(viewAdapter);
    }
}